package com.accelario.ccn.validator;

import org.junit.Assert;
import org.junit.Test;

public class LuhnCcnValidatorTest {

    @Test
    public void testTooShortString() throws Exception{
        // GIVEN validator
        CcnValidator validator = new LuhnCcnValidator();

        // AND too short ccn number
        String ccn = "123";

        // WHEN
        boolean valid = validator.isValid(ccn);

        // THEN
        Assert.assertFalse("Too short string", valid);
    }

    @Test
    public void testTooLongString() throws Exception{
        // GIVEN validator
        CcnValidator validator = new LuhnCcnValidator();

        // AND too long ccn number
        String ccn = "1234567891011121314151617181902";

        // WHEN
        boolean valid = validator.isValid(ccn);

        // THEN
        Assert.assertFalse("Too long number", valid);
    }

    @Test
    public void testStringWithChars() throws Exception{
        // GIVEN validator
        CcnValidator validator = new LuhnCcnValidator();

        // AND string with letters
        String ccn = "a12345678910111";

        // WHEN
        boolean valid = validator.isValid(ccn);

        // THEN
        Assert.assertFalse("Contains letters", valid);
    }

    @Test
    public void testStringWithSpecSymbols() throws Exception{
        // GIVEN validator
        CcnValidator validator = new LuhnCcnValidator();

        // AND string with #
        String ccn = "#12345678910111";

        // WHEN
        boolean valid = validator.isValid(ccn);

        // THEN
        Assert.assertFalse("Contains special symbols", valid);
    }

    @Test
    public void testStringWithRandomNumber() throws Exception{
        // GIVEN validator
        CcnValidator validator = new LuhnCcnValidator();

        // AND string of random numbers
        String ccn = "123456789123456";

        // WHEN
        boolean valid = validator.isValid(ccn);

        // THEN
        Assert.assertFalse("Doesn't path Luhn check", valid);
    }

    @Test
    public void testValidNumber() throws Exception{
        // GIVEN validator
        CcnValidator validator = new LuhnCcnValidator();

        // AND string of random numbers
        String ccn = "5457623898234113";

        // WHEN
        boolean valid = validator.isValid(ccn);

        // THEN
        Assert.assertTrue("Valid number", valid);
    }

}
