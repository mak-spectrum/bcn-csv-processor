package com.accelario.ccn.decoder;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.accelario.ccn.model.DataRecord;
import org.junit.Assert;
import org.junit.Test;

public class ApacheCommonsCsvDecoderTest {

    private static final List<String> CORRECT_FILE_CONTENT = Arrays.asList(
            "AAA,BBB,CCC,DDD,EEE,FFF",
            "aaa,bbb,ccc,ddd,111,fff",
            "",
            "asd,gas,crs,135,afd,5af",
            "145,856,125,685,864,411"
    );

    private static final List<String> BROKEN_FILE_CONTENT = Arrays.asList(
            "AAA,BBB,CCC,DDD,EEE,FFF",
            "\"\"\"\"aaa,bbb,ccc\",ddd\",111,fff\"\"\"\"",
            "asd,gas,crs,135,afd,5af",
            "145,856,125,685,864,411"
    );

    @Test
    public void testDecodingCorrectFile() throws Exception{
        // GIVEN decoder
        CsvDecoder decoder = new ApacheCommonsCsvDecoder();

        // WHEN correct content is parsed
        List<DataRecord> result = decoder.parseDataBlock(CORRECT_FILE_CONTENT);

        // THEN expected to see parsed content with empty lines ignoreds
        Assert.assertEquals(4, result.size());
        Assert.assertEquals(new DataRecord(new String[] {"AAA", "BBB", "CCC", "DDD", "EEE", "FFF"}), result.get(0));
        Assert.assertEquals(new DataRecord(new String[] {"aaa", "bbb", "ccc", "ddd", "111", "fff"}), result.get(1));
        Assert.assertEquals(new DataRecord(new String[] {"asd", "gas", "crs", "135", "afd", "5af"}), result.get(2));
        Assert.assertEquals(new DataRecord(new String[] {"145", "856", "125", "685", "864", "411"}), result.get(3));
    }

    @Test(expected = IOException.class)
    public void testDecodingBrokenFile() throws Exception{
        // GIVEN decoder
        CsvDecoder decoder = new ApacheCommonsCsvDecoder();

        // WHEN correct content is parsed
        List<DataRecord> result = decoder.parseDataBlock(BROKEN_FILE_CONTENT);

        // THEN exception
    }

}
