package com.accelario.ccn.encoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.accelario.ccn.model.DataRecord;
import org.apache.commons.csv.CSVFormat;
import org.junit.Assert;
import org.junit.Test;

public class ApacheCommonsCsvEncoderTest {

    private static final String EXPECTED_FILE_CONTENT = String.join(
            CSVFormat.DEFAULT.getRecordSeparator(),
            "AAA,BBB,CCC,DDD,EEE,FFF",
            "aaa,bbb,ccc,ddd,111,fff",
            "asd,gas,crs,135,afd,5af",
            "145,856,125,685,864,411",
            ""
    );

    @Test
    public void testEncodingDataBlock() throws Exception{
        // GIVEN encoder
        CsvEncoder encoder = new ApacheCommonsCsvEncoder();

        // AND data block
        List<DataRecord> list = new ArrayList<>();
        list.add(new DataRecord(new String[] {"AAA", "BBB", "CCC", "DDD", "EEE", "FFF"}));
        list.add(new DataRecord(new String[] {"aaa", "bbb", "ccc", "ddd", "111", "fff"}));
        list.add(new DataRecord(new String[] {"asd", "gas", "crs", "135", "afd", "5af"}));
        list.add(new DataRecord(new String[] {"145", "856", "125", "685", "864", "411"}));

        // WHEN correct content is encoded
        String result = encoder.encodeDataBlock(list);

        // THEN
        Assert.assertEquals(EXPECTED_FILE_CONTENT, result);
    }

    @Test
    public void testEncodingEmptyDataBlock() throws Exception{
        // GIVEN encoder
        CsvEncoder encoder = new ApacheCommonsCsvEncoder();

        // WHEN empty block encoded
        String result = encoder.encodeDataBlock(Collections.emptyList());

        // THEN
        Assert.assertEquals("", result);
    }

}
