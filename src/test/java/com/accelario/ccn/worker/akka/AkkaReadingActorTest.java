package com.accelario.ccn.worker.akka;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.ActorRefRoutee;
import akka.routing.RandomRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import akka.testkit.javadsl.TestKit;
import com.accelario.ccn.model.FileBlockInfo;
import com.accelario.ccn.worker.message.LaunchSignal;
import com.accelario.ccn.worker.message.RawDataBlock;
import com.accelario.ccn.worker.message.WorkScope;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class AkkaReadingActorTest {

    /*
     * Static test file content that is copied to each test file.
     * */
    private static final List<String> TEST_FILE_CONTENT = Arrays.asList(
            "AAA, BBB, CCC, DDD, EEE, FFF",
            "aaa, bbb, ccc, ddd, 111, fff",
            "asd, gas, crs, 135, afd, 5af",
            "145, 856, 125, 685, 864, 411"
    );

    /*
     * Number for test files that are emulated for the test.
     * */
    private static final int TEST_FILES_COUNT = 5;


    private static ActorSystem system;

    @Rule
    public TemporaryFolder workspace = new TemporaryFolder(new File("."));
    private List<String> testFileNames;

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @Before
    public void before() {
        testFileNames = new ArrayList<>();
        for (int i = 0; i < TEST_FILES_COUNT; i++) {
            testFileNames.add("testFile" + i + ".csv");
        }

        testFileNames.forEach(filename -> {
            try {
                FileUtils.copyInputStreamToFile(
                        new ByteArrayInputStream(String.join(System.lineSeparator(), TEST_FILE_CONTENT).getBytes()),
                        workspace.newFile(filename)
                );
            } catch (IOException e) {
                throw new RuntimeException(String.format("Error setting up test file [%s].", filename));
            }
        });
    }

    @Test
    public void testSupervisorLaunch() {
        new TestKit(system) {{

            // GIVEN
            final TestKit probe = new TestKit(system);

            // AND the test kit is the final message collector
            List<Routee> routees = new ArrayList<>();
            routees.add(new ActorRefRoutee(probe.getRef()));
            Router testKitRouter = new Router(new RandomRoutingLogic(), routees);

            // AND subject actor
            final ActorRef subject = system.actorOf(
                    DataReadingSupervisor.props(workspace.getRoot(), testKitRouter, probe.getRef()),
                    "readingSupervisor"
            );

            // AND expected messages: RawDataBlock with data read from each file; one block per file
            RawDataBlock[] expectedMessages = testFileNames.stream().map(filename ->
                    new RawDataBlock(
                            new FileBlockInfo(filename, 0, true),
                            TEST_FILE_CONTENT
                    )
            ).toArray(RawDataBlock[]::new);

            within(Duration.ofSeconds(3), () -> {
                // WHEN
                subject.tell(new LaunchSignal(), probe.getRef());

                // THEN
                probe.expectMsg(new WorkScope(new HashSet<>(testFileNames)));

                // THEN
                probe.expectMsgAllOf(expectedMessages);

                // AND expect no other message
                expectNoMessage();
                return null;
            });
        }};
    }

}
