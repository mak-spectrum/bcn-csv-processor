package com.accelario.ccn.worker.akka;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.ActorRefRoutee;
import akka.routing.RandomRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import akka.testkit.javadsl.TestKit;
import com.accelario.ccn.decoder.CsvDecoder;
import com.accelario.ccn.model.DataRecord;
import com.accelario.ccn.model.FileBlockInfo;
import com.accelario.ccn.model.FileStatus;
import com.accelario.ccn.worker.message.DropFile;
import com.accelario.ccn.worker.message.RawDataBlock;
import com.accelario.ccn.worker.message.RegisterCoworkers;
import com.accelario.ccn.worker.message.TableDataBlock;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class AkkaDecodingActorTest {

    private static ActorSystem system;

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @Test
    public void testSuccessfulDecoding() {
        new TestKit(system) {{

            // GIVEN
            final TestKit probe = new TestKit(system);

            // AND the test kit is the final message collector
            List<Routee> routees = new ArrayList<>();
            routees.add(new ActorRefRoutee(probe.getRef()));
            Router testKitRouter = new Router(new RandomRoutingLogic(), routees);

            // AND do nothing CsvDecoder
            List<DataRecord> fakeDataRecords = Collections.singletonList(
                    new DataRecord(new String[] {"Hello", "World"})
            );
            CsvDecoder decoder = (List<String> rawData) -> fakeDataRecords;

            // AND subject actor
            final ActorRef subject = system.actorOf(
                    DataDecodingActor.props(decoder, testKitRouter),
                    "successDecodingActor"
            );

            // AND outgoing & expected messages
            FileBlockInfo blockInfo = new FileBlockInfo("test.csv", 0, false);
            RawDataBlock outgoingMessage = new RawDataBlock(blockInfo, Collections.emptyList());

            TableDataBlock expectedMessage = new TableDataBlock(blockInfo, fakeDataRecords);

            within(Duration.ofSeconds(3), () -> {
                // WHEN
                subject.tell(outgoingMessage, probe.getRef());

                // THEN
                probe.expectMsg(expectedMessage);

                // AND expect no other message
                expectNoMessage();
                return null;
            });
        }};
    }

    @Test
    public void testErroneousDecoding() {
        new TestKit(system) {{

            // GIVEN
            final TestKit probe = new TestKit(system);

            // AND the test kit is the final message collector
            Router coworkers = new Router(
                    new RandomRoutingLogic(),
                    Collections.singletonList(new ActorRefRoutee(probe.getRef()))
            );
            Router empty = new Router(new RandomRoutingLogic());

            // AND throwing errors CsvDecoder
            CsvDecoder decoder = new CsvDecoder() {
                @Override
                public List<DataRecord> parseDataBlock(List<String> rawData) throws IOException {
                    throw new IOException("because I want");
                }
            };

            // AND subject actor
            final ActorRef subject = system.actorOf(
                    DataDecodingActor.props(decoder, empty),
                    "erroneousDecodingActor"
            );
            subject.tell(new RegisterCoworkers(coworkers), probe.getRef());

            // AND outgoing & expected messages
            FileBlockInfo blockInfo = new FileBlockInfo("test.csv", 0, false);
            RawDataBlock outgoingMessage = new RawDataBlock(blockInfo, Collections.emptyList());

            within(Duration.ofSeconds(3), () -> {
                // WHEN
                subject.tell(outgoingMessage, probe.getRef());

                // THEN
                probe.expectMsgAllOf(new DropFile(blockInfo.getFilename(), FileStatus.ERROR_DECODING));

                // AND expect no other message
                expectNoMessage();
                return null;
            });
        }};
    }

}
