package com.accelario.ccn.worker.akka;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.ActorRefRoutee;
import akka.routing.RandomRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import akka.testkit.javadsl.TestKit;
import com.accelario.ccn.model.DataRecord;
import com.accelario.ccn.model.FileBlockInfo;
import com.accelario.ccn.validator.CcnValidator;
import com.accelario.ccn.worker.message.RefinedDataBlock;
import com.accelario.ccn.worker.message.TableDataBlock;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class AkkaRefiningActorTest {

    private static ActorSystem system;

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @Test
    public void testSuccessfulRefining() {
        new TestKit(system) {{

            // GIVEN
            final TestKit probe = new TestKit(system);

            // AND the test kit is the final message collector
            List<Routee> routees = new ArrayList<>();
            routees.add(new ActorRefRoutee(probe.getRef()));
            Router testKitRouter = new Router(new RandomRoutingLogic(), routees);

            // AND special-number-valid validator
            CcnValidator validator = "11223334455"::equals;

            // AND subject actor
            final ActorRef subject = system.actorOf(
                    DataRefiningActor.props(validator, testKitRouter),
                    "refiningActor"
            );

            // AND outgoing & expected messages
            FileBlockInfo blockInfo = new FileBlockInfo("test.csv", 0, false);
            List<DataRecord> fakeDataRecords = Arrays.asList(
                    new DataRecord(new String[] {"Alpha", "World"}),
                    new DataRecord(new String[] {"Beta", "Zone", "11223334455"}),
                    new DataRecord(new String[] {"Gamma", "Dawn"}),
                    new DataRecord(new String[] {"Delta", "Force", "11223334455"})
            );
            TableDataBlock outgoingMessage = new TableDataBlock(blockInfo, fakeDataRecords);

            List<DataRecord> validRecords = Arrays.asList(
                    new DataRecord(new String[] {"Beta", "Zone", "11223334455"}),
                    new DataRecord(new String[] {"Delta", "Force", "11223334455"})
            );
            RefinedDataBlock expectedMessage = new RefinedDataBlock(blockInfo, validRecords);

            within(Duration.ofSeconds(3), () -> {
                // WHEN
                subject.tell(outgoingMessage, probe.getRef());

                // THEN
                probe.expectMsg(expectedMessage);

                // AND expect no other message
                expectNoMessage();

                return null;
            });
        }};
    }

}
