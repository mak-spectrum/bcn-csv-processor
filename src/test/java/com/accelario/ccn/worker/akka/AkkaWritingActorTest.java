package com.accelario.ccn.worker.akka;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;
import com.accelario.ccn.model.DataRecord;
import com.accelario.ccn.model.FileBlockInfo;
import com.accelario.ccn.worker.message.DoneFile;
import com.accelario.ccn.worker.message.RefinedDataBlock;
import org.apache.commons.io.IOUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class AkkaWritingActorTest {

    private static final List<DataRecord> TEST_DATA_BLOCK1 = Arrays.asList(
            new DataRecord(new String[] {"AAA", "BBB", "CCC", "DDD", "EEE", "FFF"}),
            new DataRecord(new String[] {"aaa", "bbb", "ccc", "ddd", "111", "fff"})

    );

    private static final List<DataRecord> TEST_DATA_BLOCK2 = Arrays.asList(
            new DataRecord(new String[] {"asd", "gas", "crs", "135", "afd", "5af"}),
            new DataRecord(new String[] {"145", "856", "125", "685", "864", "411"})
    );

    private static final String TEST_FILE1 = "test1.csv";
    private static final String TEST_FILE2 = "test2.csv";

    private static ActorSystem system;

    @Rule
    public TemporaryFolder workspace = new TemporaryFolder(new File("."));

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @Test
    public void testWriteTwoFiles() throws Exception {
        new TestKit(system) {{

            // GIVEN
            final TestKit probe = new TestKit(system);

            // AND subject actor
            final ActorRef subject = system.actorOf(
                    DataWritingActor.props(workspace.getRoot(), probe.getRef()),
                    "writingTwoFilesActor"
            );

            // AND outgoing messages
            RefinedDataBlock file1Block0 = new RefinedDataBlock(
                    new FileBlockInfo(TEST_FILE1, 0, false),
                    TEST_DATA_BLOCK1
            );
            RefinedDataBlock file1Block1 = new RefinedDataBlock(
                    new FileBlockInfo(TEST_FILE1, 1, false),
                    Collections.emptyList()
            );
            RefinedDataBlock file1Block2 = new RefinedDataBlock(
                    new FileBlockInfo(TEST_FILE1, 2, true),
                    TEST_DATA_BLOCK2
            );

            RefinedDataBlock file2Block0 = new RefinedDataBlock(
                    new FileBlockInfo(TEST_FILE2, 0, false),
                    Collections.emptyList()
            );
            RefinedDataBlock file2Block1 = new RefinedDataBlock(
                    new FileBlockInfo(TEST_FILE2, 1, false),
                    TEST_DATA_BLOCK2
            );
            RefinedDataBlock file2Block2 = new RefinedDataBlock(
                    new FileBlockInfo(TEST_FILE2, 2, true),
                    TEST_DATA_BLOCK1
            );

            within(Duration.ofSeconds(3), () -> {
                // WHEN
                subject.tell(file1Block0, probe.getRef());
                subject.tell(file1Block2, probe.getRef());
                subject.tell(file2Block0, probe.getRef());
                subject.tell(file1Block1, probe.getRef());
                subject.tell(file2Block1, probe.getRef());
                subject.tell(file2Block2, probe.getRef());


                // THEN expect no message
                probe.expectMsgAllOf(
                        new DoneFile(TEST_FILE1, 4),
                        new DoneFile(TEST_FILE2, 4)
                );

                // THEN expect no message
                expectNoMessage();
                return null;
            });
        }};

        // AND THEN file content is correct
        try (BufferedReader r = new BufferedReader(new FileReader(new File(workspace.getRoot(), TEST_FILE1)))) {
            String firstFileContent1 = String.join(
                    System.lineSeparator(),
                    IOUtils.readLines(r)
            );
            Assert.assertEquals(
                    "AAA,BBB,CCC,DDD,EEE,FFF" + System.lineSeparator()
                    + "aaa,bbb,ccc,ddd,111,fff" + System.lineSeparator()
                    + "asd,gas,crs,135,afd,5af" + System.lineSeparator()
                    + "145,856,125,685,864,411",
                    firstFileContent1);
        }

        try (BufferedReader r = new BufferedReader(new FileReader(new File(workspace.getRoot(), TEST_FILE2)))) {
            String firstFileContent2 = String.join(
                    System.lineSeparator(),
                    IOUtils.readLines(r)
            );
            Assert.assertEquals(
                    "asd,gas,crs,135,afd,5af" + System.lineSeparator()
                    + "145,856,125,685,864,411"  + System.lineSeparator()
                    + "AAA,BBB,CCC,DDD,EEE,FFF" + System.lineSeparator()
                    + "aaa,bbb,ccc,ddd,111,fff",
                    firstFileContent2);
        }
    }

}
