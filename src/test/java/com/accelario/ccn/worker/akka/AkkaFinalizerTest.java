package com.accelario.ccn.worker.akka;

import java.time.Duration;
import java.util.Arrays;
import java.util.HashSet;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.RandomRoutingLogic;
import akka.routing.Router;
import akka.testkit.javadsl.TestKit;
import com.accelario.ccn.finalizer.ReportPublisher;
import com.accelario.ccn.finalizer.Terminator;
import com.accelario.ccn.model.FileStatus;
import com.accelario.ccn.worker.message.DoneFile;
import com.accelario.ccn.worker.message.DropFile;
import com.accelario.ccn.worker.message.RegisterCoworkers;
import com.accelario.ccn.worker.message.WorkScope;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class AkkaFinalizerTest {

    private static final String TEST_FILE1 = "test1.csv";
    private static final String TEST_FILE2 = "test2.csv";
    private static final String TEST_FILE3 = "test3.csv";
    private static final String TEST_FILE4 = "test4.csv";
    private static final String TEST_FILE5 = "test5.csv";
    private static final String TEST_FILE6 = "test6.csv";

    private static ActorSystem system;

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @Test
    public void testFinalizeFileCopying() {
        // GIVEN report saver to catch results
        ReportSaver reportSaver = new ReportSaver();

        new TestKit(system) {{

            // GIVEN
            final TestKit probe = new TestKit(system);

            // AND do-nothing terminator
            Terminator peacefulTerminator = (system) -> { /* do nothing */ };

            // AND no coworkers
            Router empty = new Router(new RandomRoutingLogic());

            // AND subject actor
            final ActorRef subject = system.actorOf(
                    FinalizerActor.props(peacefulTerminator, reportSaver),
                    "finalizer"
            );
            subject.tell(new RegisterCoworkers(empty), ActorRef.noSender());

            // AND outgoing messages
            WorkScope workScope = new WorkScope(
                    new HashSet<>(
                            Arrays.asList(
                                TEST_FILE1,
                                TEST_FILE2,
                                TEST_FILE3,
                                TEST_FILE4,
                                TEST_FILE6,
                                TEST_FILE5
                            )
                    )
            );

            DropFile file1 = new DropFile(TEST_FILE1, FileStatus.ERROR_READING);
            DoneFile file2 = new DoneFile(TEST_FILE2, 0);
            DoneFile file3 = new DoneFile(TEST_FILE3, 16);
            DropFile file4 = new DropFile(TEST_FILE4, FileStatus.ERROR_DECODING);
            DoneFile file5 = new DoneFile(TEST_FILE5, 256);
            DropFile file6 = new DropFile(TEST_FILE6, FileStatus.ERROR_WRITING);

            within(Duration.ofSeconds(3), () -> {
                // WHEN
                subject.tell(workScope, probe.getRef());
                subject.tell(file1, probe.getRef());
                subject.tell(file2, probe.getRef());
                subject.tell(file3, probe.getRef());
                subject.tell(file4, probe.getRef());
                subject.tell(file5, probe.getRef());
                subject.tell(file6, probe.getRef());

                // THEN expect no message
                expectNoMessage();
                return null;
            });
        }};

        // AND THEN report is ready
        Assert.assertEquals(
                System.lineSeparator()
                + "==================================================" + System.lineSeparator()
                + "CSV processing finished" + System.lineSeparator()
                + "Read files: 6" + System.lineSeparator()
                + "Copied Files: 2"  + System.lineSeparator()
                + "Processed Files Without CCN: 1" + System.lineSeparator()
                + "Dropped files: 3" + System.lineSeparator()
                + "==================================================",
                reportSaver.report);
    }

    private static class ReportSaver implements ReportPublisher {
        String report;

        @Override
        public void publishFinalReport(String report) {
            this.report = report;
        }
    }
}
