package com.accelario.ccn.worker.akka;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;
import com.accelario.ccn.AkkaBootstrapUtility;
import com.accelario.ccn.worker.message.LaunchSignal;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.io.IOUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class DefaultActorSetupIntegrationTest {

    private static final Random r = new Random();
    private static ActorSystem system;
    private static List<String> validCardNumbers;

    private static final String TEST_FILE1 = "test1.csv";
    private static final String TEST_FILE2 = "test2.csv";
    private static final String TEST_FILE3 = "test3.csv";
    private static final String TEST_FILE4 = "test4.csv";
    private static final String TEST_FILE5 = "test5.csv";

    @Rule
    public TemporaryFolder source = new TemporaryFolder(new File("."));
    @Rule
    public TemporaryFolder destination = new TemporaryFolder(new File("."));


    @BeforeClass
    public static void setup() throws Exception {
        system = ActorSystem.create();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                DefaultActorSetupIntegrationTest.class.getResourceAsStream("/scraps/valid-card-numbers.txt")))) {
            validCardNumbers = br.lines()
                    .map(String::trim)
                    .filter(line -> !line.isEmpty())
                    .collect(Collectors.toList());
        }
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }



    @Test
    public void testProcessing() throws Exception {

        // test file of 2MB with 100 CCNs; one CCN per 100 lines; no breaking symbol
        generateSourceFile(TEST_FILE1, 2 * 1024 * 1024, 100, 100, false);
        // test file of 200KB with 50 CCNs; one CCN per 40 lines; no breaking symbol
        generateSourceFile(TEST_FILE2, 200 * 1024, 50, 40, false);
        // test file of 200KB with 10 CCNs; one CCN per 20 lines; with breaking symbol
        generateSourceFile(TEST_FILE3, 200 * 1024, 10, 20, true);
        // test file of 20KB with 0 CCNs; no breaking symbol
        generateSourceFile(TEST_FILE4, 20 * 1024, 0, 10, false);
        // test file of 20KB with 0 CCNs; with breaking symbol
        generateSourceFile(TEST_FILE5, 20 * 1024, 0, 10, true);

        // GIVEN default setup of actors system and the root
        ActorRef root = AkkaBootstrapUtility.setupAkkaActors(
                source.getRoot(),
                destination.getRoot(),
                system
        );

        // WHEN started
        root.tell(new LaunchSignal(), ActorRef.noSender());

        // AND finished
        system.getWhenTerminated().toCompletableFuture().get();

        // THEN check result files
        Assert.assertTrue("First file exists", new File(destination.getRoot(), TEST_FILE1).exists());
        Assert.assertEquals("Contains 100 lines with CCNs", 100, readDestinationFileContent(TEST_FILE1).size());

        Assert.assertTrue("Second file exists", new File(destination.getRoot(), TEST_FILE2).exists());
        Assert.assertEquals("Contains 50 lines with CCNs", 50, readDestinationFileContent(TEST_FILE2).size());

        Assert.assertFalse("Third file was broken", new File(destination.getRoot(), TEST_FILE3).exists());

        Assert.assertFalse("Fourth file was empty", new File(destination.getRoot(), TEST_FILE4).exists());

        Assert.assertFalse("Fifth file was empty", new File(destination.getRoot(), TEST_FILE5).exists());
    }

    private void generateSourceFile(
            String filename,
            long contentLength,
            int ccnCount,
            int ccnPerLines,
            boolean injectBrakingSymbol)
            throws Exception {

        File file = this.source.newFile(filename);

        LinkedList<String> ccns = new LinkedList<>(validCardNumbers.subList(0, ccnCount));

        try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {

            String breakingSymbol = null;
            // expected 100 bytes per line
            // breaking symbol must be in the middle
            int breakingSymbolLine = (int) contentLength / 100 / 2;
            if (injectBrakingSymbol) {
                breakingSymbol = "\"\"\"\"a";
            }

            int lineCounter = 0;
            long byteCounter = 0;
            while (byteCounter < contentLength) {
                String injection = null;

                if (!ccns.isEmpty() && lineCounter % ccnPerLines == 0) {
                    injection = ccns.poll();
                }

                if (breakingSymbol != null && injection == null && lineCounter > breakingSymbolLine) {
                    injection = breakingSymbol;
                    breakingSymbol = null;
                }

                byte[] line = this.generateRandomCsvLine(injection).getBytes();
                out.write(line);
                out.write(CSVFormat.DEFAULT.getRecordSeparator().getBytes());
                byteCounter += line.length;
                lineCounter++;
            }

        }

    }

    private String generateRandomCsvLine(String injection) {
        List<String> words = new ArrayList<>();
        for (int i = 0; i < 8 + r.nextInt(7); i++) {
            words.add(generateRandomWord());
        }

        if (injection != null) {
            words.add(r.nextInt(words.size()), injection);
        }

        return String.join(",", words);
    }

    private String generateRandomWord() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 7 + r.nextInt(5); i++) {
            sb.append((char)('A' + r.nextInt(26)));
        }
        return sb.toString();
    }

    private List<String> readDestinationFileContent(String filename) throws Exception {
        try (BufferedReader r = new BufferedReader(new FileReader(new File(destination.getRoot(), filename)))) {
            return IOUtils.readLines(r);
        }
    }
}
