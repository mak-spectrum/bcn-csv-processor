package com.accelario.ccn;

import java.io.File;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.accelario.ccn.worker.message.LaunchSignal;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
//        args = new String[] {"./test-local/", "./test-result/akka/"};
        if (!validateArguments(args)) {
            return;
        }

        File sourceFolder = new File(args[0]);
        File destinationFolder = initDestinationFolder(args[1]);

        performCopying(sourceFolder, destinationFolder);
    }

    private static File initDestinationFolder(String arg) {
        File destinationFolder = new File(arg);
        if (!destinationFolder.exists()) {
            try {
                FileUtils.forceMkdir(destinationFolder);
            } catch (Exception e) {
                LOGGER.error("Error creating destination folder", e);
            }
        }
        return destinationFolder;
    }

    private static void performCopying(File sourceFolder, File destinationFolder) {
        final ActorSystem system = ActorSystem.create("ccn");
        try {
            ActorRef rootActor = AkkaBootstrapUtility.setupAkkaActors(sourceFolder, destinationFolder, system);
            // start
            rootActor.tell(new LaunchSignal(), ActorRef.noSender());

            system.getWhenTerminated().toCompletableFuture().get();
        } catch (Exception e) {
            LOGGER.error("Error executing data copying", e);
        } finally {
            system.terminate();
        }
    }

    private static boolean validateArguments(String[] args) {
        if (args.length < 2) {
            System.out.println("Please specify source folder and destination folder");
            return false;
        } else {
            File sourceFolder = new File(args[0]);
            if (!sourceFolder.exists() || !sourceFolder.isDirectory()) {
                System.out.println("Source folder is not a folder or doesn't exist.");
                return false;
            } else if (sourceFolder.list() == null || sourceFolder.list().length == 0) {
                System.out.println("Source folder is empty.");
                return false;
            }

            File destinationFolder = new File(args[1]);
            if (destinationFolder.exists()) {
                if (!destinationFolder.isDirectory()) {
                    System.out.println("Destination folder is not a folder");
                    return false;
                }
            } else {
                System.out.println(
                        String.format(
                                "Destination folder [%s] doesn't exist. It will be created automatically",
                                args[1]
                        )
                );
            }
        }

        return true;
    }
}
