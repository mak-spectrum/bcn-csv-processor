package com.accelario.ccn.encoder;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import com.accelario.ccn.model.DataRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

public class ApacheCommonsCsvEncoder implements CsvEncoder {

    @Override
    public String encodeDataBlock(List<DataRecord> lines) throws IOException {
        if (lines.isEmpty()) {
            return "";
        }

        StringWriter writer = new StringWriter();
        CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT);
        lines.forEach(record -> {
            try {
                List<String> line = this.recordToListCells(record);
                printer.printRecord(line);
            } catch (IOException e) {
                // this should not really happen, since data are written to in-memory writer
                throw new RuntimeException(e);
            }
        });

        return writer.toString();
    }

    private List<String> recordToListCells(DataRecord record) {
        List<String> line = new ArrayList<>();
        record.cellIterator().forEachRemaining(line::add);
        return line;
    }

}
