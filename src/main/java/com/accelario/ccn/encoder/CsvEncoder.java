package com.accelario.ccn.encoder;


import java.io.IOException;
import java.util.List;

import com.accelario.ccn.model.DataRecord;

public interface CsvEncoder {

    String encodeDataBlock(List<DataRecord> lines) throws IOException;

}
