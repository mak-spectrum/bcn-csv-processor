package com.accelario.ccn.finalizer;

import akka.actor.ActorSystem;

public class DefaultTerminator implements Terminator {

    @Override
    public void terminate(ActorSystem system) {
        system.terminate();
    }

}
