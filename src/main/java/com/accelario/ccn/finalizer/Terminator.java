package com.accelario.ccn.finalizer;

import akka.actor.ActorSystem;

public interface Terminator {

    void terminate(ActorSystem system);

}
