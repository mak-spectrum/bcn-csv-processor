package com.accelario.ccn.finalizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerReportPublisher implements ReportPublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerReportPublisher.class);

    @Override
    public void publishFinalReport(String report) {
        LOGGER.info(report);
    }
}
