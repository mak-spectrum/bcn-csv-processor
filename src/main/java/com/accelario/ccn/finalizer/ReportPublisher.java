package com.accelario.ccn.finalizer;

public interface ReportPublisher {

    void publishFinalReport(String report);

}
