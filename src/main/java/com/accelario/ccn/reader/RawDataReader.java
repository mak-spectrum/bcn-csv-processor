package com.accelario.ccn.reader;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.accelario.ccn.worker.message.RawDataBlock;
import com.accelario.ccn.model.FileBlockInfo;

public class RawDataReader implements Closeable {

    private static final int BLOCK_LENGTH_LIMIT = 256; // TODO move me to configuration
    private static final int CHAR_BUFFER_SIZE = 256 * 1024; // TODO move me to configuration


    private String filename;
    private BufferedReader dataReader;
    private int blockIndex = 0;

    public RawDataReader(File sourceFolder, String filename) throws IOException {
        this.filename = filename;
        this.dataReader = new BufferedReader(
                new FileReader(new File(sourceFolder, filename)),
                CHAR_BUFFER_SIZE);
    }

    public RawDataBlock readNextBlock() throws IOException {
        DataBlockAccumulator acc = new DataBlockAccumulator();

        int blockLength = 0;
        boolean blockLimitReached = false;
        String nextLine;

        while (!blockLimitReached && (nextLine = dataReader.readLine()) != null) {
            acc.storeLine(nextLine);
            blockLength++;

            if (blockLength >= BLOCK_LENGTH_LIMIT) {
                blockLimitReached = true;
            }
        }

        /*
         * if reading ended and blockLimitReached is false, assume it is the last block
         */
        boolean _final = !blockLimitReached;

        return new RawDataBlock(
                new FileBlockInfo(
                        this.filename,
                        blockIndex++,
                        _final
                ),
                acc.lines
        );
    }

    public void close() throws IOException {
        this.dataReader.close();
    }

    private static class DataBlockAccumulator {
        List<String> lines = new ArrayList<>();

        void storeLine(String line) {
            this.lines.add(line);
        }
    }

}
