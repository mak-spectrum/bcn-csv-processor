package com.accelario.ccn;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.ActorRefRoutee;
import akka.routing.BroadcastRoutingLogic;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import com.accelario.ccn.decoder.ApacheCommonsCsvDecoder;
import com.accelario.ccn.decoder.CsvDecoder;
import com.accelario.ccn.finalizer.DefaultTerminator;
import com.accelario.ccn.finalizer.LoggerReportPublisher;
import com.accelario.ccn.validator.CcnValidator;
import com.accelario.ccn.validator.LuhnCcnValidator;
import com.accelario.ccn.worker.akka.DataDecodingActor;
import com.accelario.ccn.worker.akka.DataReadingSupervisor;
import com.accelario.ccn.worker.akka.DataRefiningActor;
import com.accelario.ccn.worker.akka.DataWritingActor;
import com.accelario.ccn.worker.akka.FinalizerActor;
import com.accelario.ccn.worker.message.RegisterCoworkers;

public final class AkkaBootstrapUtility {

    private static final int DECODERS_COUNT = 3;
    private static final int REFINERS_COUNT = 3;


    private AkkaBootstrapUtility() {
        // Hidden constructor
    }

    public static ActorRef setupAkkaActors(
            File sourceFolder, File destinationFolder, ActorSystem system) {

        List<Routee> coworkersAccumulator = new ArrayList<>();

        ActorRef finalizer = system.actorOf(
                FinalizerActor.props(new DefaultTerminator(), new LoggerReportPublisher()),
                "finalizer"
        );
        coworkersAccumulator.add(new ActorRefRoutee(finalizer));

        Router writingRouter = configureWritingLayer(system, destinationFolder, finalizer, coworkersAccumulator);
        Router refiningRouter = configureRefiningLayer(system, writingRouter, coworkersAccumulator);
        Router decodingRouter = configureDecodingLayer(system, refiningRouter, coworkersAccumulator);

        ActorRef root = system.actorOf(
                DataReadingSupervisor.props(sourceFolder, decodingRouter, finalizer),
                "readingSupervisor"
        );
        coworkersAccumulator.add(new ActorRefRoutee(root));

        Router coworkers = new Router(new BroadcastRoutingLogic(), coworkersAccumulator);
        coworkers.route(new RegisterCoworkers(coworkers), ActorRef.noSender());

        return root;
    }

    private static Router configureDecodingLayer(ActorSystem system,
                                                 Router refiningRouter,
                                                 List<Routee> coworkersAccumulator) {

        CsvDecoder csvDecoder = new ApacheCommonsCsvDecoder();

        List<Routee> decodingRoutees = new ArrayList<>();

        for (int i = 0; i < DECODERS_COUNT; i++) {
            ActorRef decodingActor = system.actorOf(
                    DataDecodingActor.props(csvDecoder, refiningRouter),
                    "decodingActor" + i
            );
            coworkersAccumulator.add(new ActorRefRoutee(decodingActor));
            decodingRoutees.add(new ActorRefRoutee(decodingActor));
        }

        return new Router(new RoundRobinRoutingLogic(), decodingRoutees);
    }

    private static Router configureRefiningLayer(ActorSystem system,
                                                 Router writingRouter,
                                                 List<Routee> coworkersAccumulator) {

        CcnValidator ccnValidator = new LuhnCcnValidator();

        List<Routee> refiningRoutees = new ArrayList<>();

        for (int i = 0; i < REFINERS_COUNT; i++) {
            ActorRef refiningActor = system.actorOf(
                    DataRefiningActor.props(ccnValidator, writingRouter),
                    "refiningActor" + i
            );
            coworkersAccumulator.add(new ActorRefRoutee(refiningActor));
            refiningRoutees.add(new ActorRefRoutee(refiningActor));
        }

        return new Router(new RoundRobinRoutingLogic(), refiningRoutees);
    }

    private static Router configureWritingLayer(ActorSystem system,
                                                File destinationFolder,
                                                ActorRef finalizer,
                                                List<Routee> coworkersAccumulator) {

        ActorRef writingActor = system.actorOf(
                DataWritingActor.props(destinationFolder, finalizer),
                "writingActor"
        );
        coworkersAccumulator.add(new ActorRefRoutee(writingActor));

        List<Routee> writingRoutees = new ArrayList<>();
        writingRoutees.add(new ActorRefRoutee(writingActor));
        return new Router(new RoundRobinRoutingLogic(), writingRoutees);
    }
}
