package com.accelario.ccn.model;

public enum FileStatus {

    IN_PROGRESS,
    DONE,
    ERROR_READING,
    ERROR_DECODING,
    ERROR_WRITING

}
