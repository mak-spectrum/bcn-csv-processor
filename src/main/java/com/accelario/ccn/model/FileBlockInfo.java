package com.accelario.ccn.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * General information about data block that was read from a file.
 * This information is used for data consistency on each data processing step.
 * */
public class FileBlockInfo {

    private String filename;
    private int blockIndex;
    private boolean _final;

    public FileBlockInfo(String filename, int blockIndex, boolean _final) {
        this.filename = filename;
        this.blockIndex = blockIndex;
        this._final = _final;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        FileBlockInfo that = (FileBlockInfo) o;

        return new EqualsBuilder()
                .append(blockIndex, that.blockIndex)
                .append(_final, that._final)
                .append(filename, that.filename)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(filename)
                .append(blockIndex)
                .append(_final)
                .toHashCode();
    }

    public String getFilename() {
        return filename;
    }

    public int getBlockIndex() {
        return blockIndex;
    }

    public boolean isFinal() {
        return _final;
    }

    @Override
    public String toString() {
        return "FileBlockInfo{" +
                "filename='" + filename + '\'' +
                ", blockIndex=" + blockIndex +
                ", _final=" + _final +
                '}';
    }
}
