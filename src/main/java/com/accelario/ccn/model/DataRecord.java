package com.accelario.ccn.model;

import java.util.Arrays;
import java.util.Iterator;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DataRecord {

    private String[] cells;

    public DataRecord(String[] cells) {
        this.cells = cells;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DataRecord that = (DataRecord) o;

        return new EqualsBuilder()
                .append(cells, that.cells)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(cells)
                .toHashCode();
    }

    public Iterator<String> cellIterator() {
        return Arrays.stream(cells).iterator();
    }

    public String getCellValue(int cellIndex) {
        if (cellIndex < 0 || cellIndex >= cells.length) {
            throw new ArrayIndexOutOfBoundsException(cellIndex);
        }
        return cells[cellIndex];
    }

    public int getLength() {
        return cells.length;
    }

    @Override
    public String toString() {
        return "DataRecord{" +
                "cells=" + Arrays.toString(cells) +
                '}';
    }
}
