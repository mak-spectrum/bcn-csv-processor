package com.accelario.ccn.worker.message;

import akka.routing.Router;

/**
 * DependencyInjection message. Provides coworkers list for those, who may concern.
 * */
public class RegisterCoworkers {

    private Router coworkers;

    public RegisterCoworkers(Router coworkers) {
        this.coworkers = coworkers;
    }

    public Router getCoworkers() {
        return coworkers;
    }
}
