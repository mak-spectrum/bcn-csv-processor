package com.accelario.ccn.worker.message;

import java.util.Collections;
import java.util.List;

import com.accelario.ccn.model.DataRecord;
import com.accelario.ccn.model.FileBlockInfo;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Block of Table-like data. Recognized and decoded from CSV.
 * Data refining, to extract Bank Cards Numbers, from is needed, as the next step.
 * */
public class TableDataBlock {

    private FileBlockInfo blockInfo;
    private List<DataRecord> lines;

    public TableDataBlock(FileBlockInfo blockInfo, List<DataRecord> lines) {
        this.blockInfo = blockInfo;
        this.lines = Collections.unmodifiableList(lines);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TableDataBlock that = (TableDataBlock) o;

        return new EqualsBuilder()
                .append(blockInfo, that.blockInfo)
                .append(lines, that.lines)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(blockInfo)
                .append(lines)
                .toHashCode();
    }

    public FileBlockInfo getBlockInfo() {
        return blockInfo;
    }

    public List<DataRecord> getLines() {
        return lines;
    }

    @Override
    public String toString() {
        return "TableDataBlock{" +
                "blockInfo=" + blockInfo +
                ", lines=" + lines +
                '}';
    }
}
