package com.accelario.ccn.worker.message;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Reading Supervisor to child signal to start reading a file
 * */
public class ReadFile {

    private String filename;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ReadFile readFile = (ReadFile) o;

        return new EqualsBuilder()
                .append(filename, readFile.filename)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(filename)
                .toHashCode();
    }

    public ReadFile(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    @Override
    public String toString() {
        return "ReadFile{" +
                "filename='" + filename + '\'' +
                '}';
    }
}
