package com.accelario.ccn.worker.message;

import java.util.Collections;
import java.util.List;

import com.accelario.ccn.model.FileBlockInfo;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Raw lines, obtained from a file. These lines can have any data.
 * CSV recognition and parsing is needed, as the next step.
 * */
public class RawDataBlock {

    private FileBlockInfo blockInfo;
    private List<String> lines;

    public RawDataBlock(FileBlockInfo blockInfo, List<String> lines) {
        this.blockInfo = blockInfo;
        this.lines = Collections.unmodifiableList(lines);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        RawDataBlock dataBlock = (RawDataBlock) o;

        return new EqualsBuilder()
                .append(blockInfo, dataBlock.blockInfo)
                .append(lines, dataBlock.lines)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(blockInfo)
                .append(lines)
                .toHashCode();
    }

    public FileBlockInfo getBlockInfo() {
        return blockInfo;
    }

    public List<String> getLines() {
        return lines;
    }

    @Override
    public String toString() {
        return "RawDataBlock{" +
                "blockInfo=" + blockInfo +
                ", lines=" + lines +
                '}';
    }
}
