package com.accelario.ccn.worker.message;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Signal from writer to finalizer that the file is done.
 * */
public class DoneFile {

    private String filename;
    private int extractedLinesCount;

    public DoneFile(String filename, int extractedLinesCount) {
        this.filename = filename;
        this.extractedLinesCount = extractedLinesCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DoneFile doneFile = (DoneFile) o;

        return new EqualsBuilder()
                .append(extractedLinesCount, doneFile.extractedLinesCount)
                .append(filename, doneFile.filename)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(filename)
                .append(extractedLinesCount)
                .toHashCode();
    }

    public String getFilename() {
        return filename;
    }

    public int getExtractedLinesCount() {
        return extractedLinesCount;
    }

    @Override
    public String toString() {
        return "DoneFile{" +
                "filename='" + filename + '\'' +
                ", extractedLinesCount=" + extractedLinesCount +
                '}';
    }
}
