package com.accelario.ccn.worker.message;

import java.util.Collections;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Report information about work scope, provided from the reading supervisor to the finalizer.
 * Finalizer does check, when the work is done.
 * */
public class WorkScope {

    private Set<String> targetFiles;

    public WorkScope(Set<String> targetFiles) {
        this.targetFiles = Collections.unmodifiableSet(targetFiles);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        WorkScope workScope = (WorkScope) o;

        return new EqualsBuilder()
                .append(targetFiles, workScope.targetFiles)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(targetFiles)
                .toHashCode();
    }

    public Set<String> getTargetFiles() {
        return targetFiles;
    }

    @Override
    public String toString() {
        return "WorkScope{" +
                "targetFiles=" + targetFiles +
                '}';
    }
}
