package com.accelario.ccn.worker.message;

import com.accelario.ccn.model.FileStatus;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * System wide signal to put a broken/inappropriate file into quarantine.
 * */
public class DropFile {

    private String filename;
    private FileStatus status;

    public DropFile(String filename, FileStatus status) {
        this.filename = filename;
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DropFile dropFile = (DropFile) o;

        return new EqualsBuilder()
                .append(filename, dropFile.filename)
                .append(status, dropFile.status)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(filename)
                .append(status)
                .toHashCode();
    }

    public String getFilename() {
        return filename;
    }

    public FileStatus getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "DropFile{" +
                "filename='" + filename + '\'' +
                ", status=" + status +
                '}';
    }
}
