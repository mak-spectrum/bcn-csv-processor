package com.accelario.ccn.worker.akka;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.Router;
import com.accelario.ccn.decoder.CsvDecoder;
import com.accelario.ccn.model.FileStatus;
import com.accelario.ccn.worker.message.DropFile;
import com.accelario.ccn.worker.message.RawDataBlock;
import com.accelario.ccn.worker.message.RegisterCoworkers;
import com.accelario.ccn.worker.message.TableDataBlock;

public class DataDecodingActor extends AbstractActor {

    public static Props props(CsvDecoder decoder, Router refiningActors) {
        return Props.create(DataDecodingActor.class,
                () -> new DataDecodingActor(decoder, refiningActors));
    }

    private final LoggingAdapter logger = Logging.getLogger(getContext().getSystem(), this);
    private final Set<String> droppedFiles = new HashSet<>();

    private final CsvDecoder decoder;
    private final Router refiningActors;
    private Router coworkers;

    DataDecodingActor(CsvDecoder decoder, Router refiningActors) {
        this.decoder = decoder;
        this.refiningActors = refiningActors;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(RawDataBlock.class, this::decodeRawDataBlock)
                .match(DropFile.class, this::registerDroppedFile)
                .match(RegisterCoworkers.class, this::registerCoworkers)
                .build();
    }

    private void decodeRawDataBlock(RawDataBlock rdb) {
        try {
            this.logger.debug("Received [{}] block of file [{}]", rdb.getBlockInfo().getBlockIndex(),
                    rdb.getBlockInfo().getFilename());

            if (this.droppedFiles.contains(rdb.getBlockInfo().getFilename())) {
                this.logger.debug("Ignored [{}] block of broken file [{}]", rdb.getBlockInfo().getBlockIndex(),
                        rdb.getBlockInfo().getFilename());

            } else {
                TableDataBlock tdb = new TableDataBlock(
                        rdb.getBlockInfo(),
                        this.decoder.parseDataBlock(rdb.getLines())
                );

                this.logger.debug("Processed [{}] block of file [{}]", rdb.getBlockInfo().getBlockIndex(),
                        rdb.getBlockInfo().getFilename());

                this.refiningActors.route(tdb, self());
            }
        } catch (IOException e) {

            this.logger.error("Error decoding file [{}]", rdb.getBlockInfo().getFilename());

            this.droppedFiles.add(rdb.getBlockInfo().getFilename());
            this.coworkers.route(
                    new DropFile(rdb.getBlockInfo().getFilename(), FileStatus.ERROR_DECODING),
                    self()
            );
        }
    }

    private void registerDroppedFile(DropFile df) {
        this.logger.debug("Register dropped file [{}]", df.getFilename());

        this.droppedFiles.add(df.getFilename());
    }

    private void registerCoworkers(RegisterCoworkers rc) {
        this.logger.debug("Register coworkers");

        this.coworkers = rc.getCoworkers();
    }

}
