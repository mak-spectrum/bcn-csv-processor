package com.accelario.ccn.worker.akka;

import java.io.File;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import akka.actor.AbstractActorWithTimers;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.ActorRefRoutee;
import akka.routing.Broadcast;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import com.accelario.ccn.worker.message.DropFile;
import com.accelario.ccn.worker.message.LaunchFile;
import com.accelario.ccn.worker.message.LaunchSignal;
import com.accelario.ccn.worker.message.ReadFile;
import com.accelario.ccn.worker.message.RegisterCoworkers;
import com.accelario.ccn.worker.message.WorkScope;

public class DataReadingSupervisor extends AbstractActorWithTimers {

    private static final int CHILD_READERS_COUNT = 1; // TODO move me to configuration

    public static Props props(File sourceFolder, Router decodingActors, ActorRef finalizer) {
        return Props.create(DataReadingSupervisor.class,
                () -> new DataReadingSupervisor(sourceFolder, decodingActors, finalizer));
    }

    private final LoggingAdapter logger = Logging.getLogger(getContext().getSystem(), this);
    private Router childWorkers;
    private Queue<String> sourceFilesQueue;

    private final File sourceFolder;
    private final Router decodingActors;
    private final ActorRef finalizer;
    private Router coworkers;

    private DataReadingSupervisor(File sourceFolder, Router decodingActors, ActorRef finalizer) {
        this.sourceFolder = sourceFolder;
        this.decodingActors = decodingActors;
        this.finalizer = finalizer;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(LaunchSignal.class, ls -> {
                    this.childWorkers = this.prepareChildWorkers(
                            this.sourceFolder, this.decodingActors, this.coworkers);

                    this.sourceFilesQueue = this.prepareSourceFiles(this.sourceFolder);

                    this.finalizer.tell(new WorkScope(new HashSet<>(this.sourceFilesQueue)), self());

                    timers().startSingleTimer("LAUNCH_FILE", new LaunchFile(), Duration.ofMillis(500));
                })
                .match(LaunchFile.class, ls -> {
                    if (!this.sourceFilesQueue.isEmpty()) {
                        String nextFile = this.sourceFilesQueue.poll();

                        this.logger.info("Launching file [{}].", nextFile);

                        this.childWorkers.route(new ReadFile(nextFile), self());

                        self().tell(new LaunchFile(), self());
                    }
                })
                .match(DropFile.class, this::registerDroppedFile)
                .match(RegisterCoworkers.class, this::registerCoworkers)
                .build();
    }

    private Router prepareChildWorkers(File sourceFolder, Router decodingActors, Router coworkers) {
        List<Routee> routees = new ArrayList<>();
        for (int i = 0; i < CHILD_READERS_COUNT; i++) {
            routees.add(new ActorRefRoutee(
                    context().actorOf(
                            DataReadingActor.props(sourceFolder, decodingActors, coworkers),
                            "dataReader" + i
                    )
            ));
        }

        return new Router(new RoundRobinRoutingLogic(), routees);
    }

    private Queue<String> prepareSourceFiles(File sourceFolder) {
        String[] filenames = sourceFolder.list();
        if (filenames != null) {
            return new LinkedList<>(Arrays.asList(filenames));
        } else {
            return new LinkedList<>();
        }
    }

    private void registerDroppedFile(DropFile df) {
        this.logger.debug("Register dropped file [{}]", df.getFilename());

        this.childWorkers.route(new Broadcast(df), sender());
    }

    private void registerCoworkers(RegisterCoworkers rc) {
        this.logger.debug("Register coworkers");

        this.coworkers = rc.getCoworkers();
    }
}
