package com.accelario.ccn.worker.akka;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.Router;
import com.accelario.ccn.finalizer.ReportPublisher;
import com.accelario.ccn.finalizer.Terminator;
import com.accelario.ccn.model.FileStatus;
import com.accelario.ccn.worker.message.DoneFile;
import com.accelario.ccn.worker.message.DropFile;
import com.accelario.ccn.worker.message.RegisterCoworkers;
import com.accelario.ccn.worker.message.WorkScope;

public class FinalizerActor extends AbstractActor {

    public static Props props(Terminator terminator, ReportPublisher publisher) {
        return Props.create(FinalizerActor.class,
                () -> new FinalizerActor(terminator, publisher));
    }

    private final LoggingAdapter logger = Logging.getLogger(getContext().getSystem(), this);

    private Router coworkers;
    private Map<String, FileState> filesRegistry = Collections.emptyMap();

    private final Terminator terminator;
    private final ReportPublisher publisher;

    FinalizerActor(Terminator terminator, ReportPublisher publisher) {
        this.terminator = terminator;
        this.publisher = publisher;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(WorkScope.class, this::registerWorkScope)
                .match(DoneFile.class, this::registerDoneFile)
                .match(DropFile.class, this::registerDroppedFile)
                .match(RegisterCoworkers.class, this::registerCoworkers)
                .build();
    }

    private void registerWorkScope(WorkScope ws) {
        this.logger.warning("Register work scope");

        this.filesRegistry = new HashMap<>();
        ws.getTargetFiles().forEach(filename ->
                this.filesRegistry.put(filename, new FileState(FileStatus.IN_PROGRESS))
        );
    }

    private void registerDoneFile(DoneFile df) {
        this.logger.debug("Register done file [{}]", df.getFilename());

        FileState state = this.filesRegistry.get(df.getFilename());
        if (state != null) {
            state.status = FileStatus.DONE;
            state.extractedLinesCount = df.getExtractedLinesCount();
            this.checkFinalState();
        } else {
            throw new IllegalStateException(String.format("Found unexpected file [%s]", df.getFilename()));
        }
    }

    private void registerDroppedFile(DropFile df) {
        this.logger.debug("Register dropped file [{}]", df.getFilename());

        FileState state = this.filesRegistry.get(df.getFilename());
        if (state != null) {
            state.status = df.getStatus();
            this.checkFinalState();
        } else {
            throw new IllegalStateException(String.format("Found unexpected file [%s]", df.getFilename()));
        }
    }

    private void checkFinalState() {
        if (!hasUnfinishedFiles()) {
            this.logger.info("All files have been copied. Shutdown.");
            this.coworkers.route(PoisonPill.getInstance(), ActorRef.noSender());

            try { Thread.sleep(1000); } catch (Exception e) { logger.debug("Termination sleep interrupted"); }
            this.prepareFinalReport();
            this.terminator.terminate(context().system());
        }
    }

    private void prepareFinalReport() {
        FileReport report = this.prepareReport();
        StringBuilder finalInfo = new StringBuilder(System.lineSeparator())
                .append("==================================================").append(System.lineSeparator())
                .append("CSV processing finished").append(System.lineSeparator())
                .append("Read files: ").append(report.readFiles).append(System.lineSeparator())
                .append("Copied Files: ").append(report.processedFiles)
                .append(System.lineSeparator())
                .append("Processed Files Without CCN: ").append(report.processedFilesWithoutCcn)
                .append(System.lineSeparator())
                .append("Dropped files: ").append(report.droppedFiles)
                .append(System.lineSeparator())
                .append("==================================================");

        this.publisher.publishFinalReport(finalInfo.toString());
    }

    private boolean hasUnfinishedFiles() {
        for (Map.Entry<String, FileState> file : this.filesRegistry.entrySet()) {
            if (file.getValue().status == FileStatus.IN_PROGRESS) {
                logger.debug("Unfinished file [{}]", file.getKey());
                return true;
            }
        }

        return false;
    }

    private FileReport prepareReport() {
        FileReport report = new FileReport(this.filesRegistry.size());
        for (Map.Entry<String, FileState> file : this.filesRegistry.entrySet()) {
            if (file.getValue().status == FileStatus.DONE) {
                if (file.getValue().extractedLinesCount > 0) {
                    report.processedFiles++;
                } else {
                    report.processedFilesWithoutCcn++;
                }
            } else {
                report.droppedFiles++;
            }
        }
        return report;
    }

    private void registerCoworkers(RegisterCoworkers rc) {
        this.logger.debug("Register coworkers");

        this.coworkers = rc.getCoworkers();
    }

    private static class FileState {

        public FileState(FileStatus status) {
            this.status = status;
            this.extractedLinesCount = 0;
        }

        public FileState(FileStatus status, int extractedLinesCount) {
            this.status = status;
            this.extractedLinesCount = extractedLinesCount;
        }

        FileStatus status;
        int extractedLinesCount;

    }

    private static class FileReport {

        public FileReport(int readFiles) {
            this.readFiles = readFiles;
        }

        int readFiles;
        int processedFiles;
        int processedFilesWithoutCcn;
        int droppedFiles;
    }

}
