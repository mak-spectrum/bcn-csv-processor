package com.accelario.ccn.worker.akka;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.Router;
import com.accelario.ccn.model.DataRecord;
import com.accelario.ccn.validator.CcnValidator;
import com.accelario.ccn.worker.message.DropFile;
import com.accelario.ccn.worker.message.RefinedDataBlock;
import com.accelario.ccn.worker.message.RegisterCoworkers;
import com.accelario.ccn.worker.message.TableDataBlock;

public class DataRefiningActor extends AbstractActor {

    public static Props props(CcnValidator validator, Router writingActors) {
        return Props.create(DataRefiningActor.class,
                () -> new DataRefiningActor(validator, writingActors));
    }

    private final LoggingAdapter logger = Logging.getLogger(getContext().getSystem(), this);
    private final Set<String> droppedFiles = new HashSet<>();

    private final CcnValidator validator;
    private final Router writingActors;

    DataRefiningActor(CcnValidator validator, Router writingActors) {
        this.validator = validator;
        this.writingActors = writingActors;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(TableDataBlock.class, this::refineDataBlock)
                .match(DropFile.class, this::registerDroppedFile)
                .match(RegisterCoworkers.class, rc -> {/* not needed */})
                .build();
    }

    private void refineDataBlock(TableDataBlock tdb) {
        this.logger.debug("Received [{}] block of file [{}]", tdb.getBlockInfo().getBlockIndex(),
                tdb.getBlockInfo().getFilename());

        if (this.droppedFiles.contains(tdb.getBlockInfo().getFilename())) {
            this.logger.debug("Ignored [{}] block of broken file [{}]", tdb.getBlockInfo().getBlockIndex(),
                    tdb.getBlockInfo().getFilename());

        } else {
            List<DataRecord> refinedLines = this.doRefineRecords(tdb);

            this.logger.debug("Refined [{}] block of file [{}]", tdb.getBlockInfo().getBlockIndex(),
                    tdb.getBlockInfo().getFilename());

            this.writingActors.route(new RefinedDataBlock(tdb.getBlockInfo(), refinedLines), self());
        }
    }

    private List<DataRecord> doRefineRecords(TableDataBlock tdb) {
        List<DataRecord> result = new ArrayList<>();

        tdb.getLines().forEach(record -> {
            for (int i = 0; i < record.getLength(); i++) {
                if (this.validator.isValid(record.getCellValue(i))) {
                    result.add(record);
                    break;
                }
            }
        });

        return result;
    }

    private void registerDroppedFile(DropFile df) {
        this.logger.debug("Register dropped file [{}]", df.getFilename());

        this.droppedFiles.add(df.getFilename());
    }

}
