package com.accelario.ccn.worker.akka;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.Router;
import com.accelario.ccn.encoder.ApacheCommonsCsvEncoder;
import com.accelario.ccn.encoder.CsvEncoder;
import com.accelario.ccn.model.FileStatus;
import com.accelario.ccn.worker.Constants;
import com.accelario.ccn.worker.message.DoneFile;
import com.accelario.ccn.worker.message.DropFile;
import com.accelario.ccn.worker.message.RefinedDataBlock;
import com.accelario.ccn.worker.message.RegisterCoworkers;

/*
 * TODO: implement possibility extending list of writers, for cases, when there are multiple HDDs.
 *
 * It can be done by having a dedicated writer per file:
 * - when writer gets first block it writes file, and assigns himself to the file;
 * - when writer gets a block of own file, it writes data;
 * - when writer gets the last block of own file, it writes data and closes it;
 * - when writer gets a foreign block of file it passes it to the sibling.
 * */
public class DataWritingActor extends AbstractActor {

    private static final int CHAR_BUFFER_SIZE = 256 * 1024; // TODO move me to configuration


    public static Props props(File destinationFolder, ActorRef finalizer) {
        return Props.create(DataWritingActor.class,
                () -> new DataWritingActor(destinationFolder, finalizer));
    }

    private final LoggingAdapter logger = Logging.getLogger(getContext().getSystem(), this);
    private final Map<String, FileWritingMeta> fileWritingMeta = new HashMap<>();
    private final CsvEncoder encoder = new ApacheCommonsCsvEncoder();
    private final Set<String> droppedFiles = new HashSet<>();

    private final File destinationFolder;
    private final ActorRef finalizer;
    private Router coworkers;

    DataWritingActor(File destinationFolder, ActorRef finalizer) {
        this.destinationFolder = destinationFolder;
        this.finalizer = finalizer;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(RefinedDataBlock.class, this::performBlockWriting)
                .match(DropFile.class, this::registerDroppedFile)
                .match(RegisterCoworkers.class, this::registerCoworkers)
                .build();
    }

    private void performBlockWriting(RefinedDataBlock rdb) {
        this.logger.debug("Received [{}] block of file [{}]", rdb.getBlockInfo().getBlockIndex(),
                rdb.getBlockInfo().getFilename());

        try {
            FileWritingMeta meta = findMeta(rdb);

            if (this.droppedFiles.contains(rdb.getBlockInfo().getFilename())) {
                if (meta != null) {
                    this.deleteDroppedFile(rdb.getBlockInfo().getFilename());
                }
            } else {
                this.processBlockByMeta(rdb, meta);
            }

        } catch (IOException e) {
            this.logger.error("Error writing file [{}]. Drop file system wide.", rdb.getBlockInfo().getFilename(), e);

            this.droppedFiles.add(rdb.getBlockInfo().getFilename());
            this.coworkers.route(
                    new DropFile(rdb.getBlockInfo().getFilename(), FileStatus.ERROR_WRITING),
                    self()
            );
        }
    }

    private void processBlockByMeta(RefinedDataBlock rdb, FileWritingMeta meta) throws IOException {
        if (meta != null) {
            if (!meta.closed) {
                if (meta.lastBlockIndex + 1 == rdb.getBlockInfo().getBlockIndex()) {
                    this.logger.debug("Writing [{}] block into file [{}]", rdb.getBlockInfo().getBlockIndex(),
                            rdb.getBlockInfo().getFilename());

                    writeData(rdb, meta);

                    meta.lastBlockIndex++;

                    if (rdb.getBlockInfo().isFinal()) {
                        this.logger.info("Finished file [{}]", rdb.getBlockInfo().getFilename());
                        meta.closed = true;
                        this.finalizer.tell(
                                new DoneFile(rdb.getBlockInfo().getFilename(), meta.extractedLinesCount),
                                self()
                        );
                    }
                } else {
                    // not ready to process this block -> return to the queue
                    this.logger.debug("Premature receiving [{}] block of file [{}]. Returning it to queue",
                            rdb.getBlockInfo().getBlockIndex(),
                            rdb.getBlockInfo().getFilename());

                    self().tell(rdb, sender());
                }
            } else {
                // this should not really occur
                throw new IllegalStateException("Cannot append a closed file [" + meta.filename + "]");
            }
        } else {
            // TODO: send to a sibling
            // not ready to process this block -> return to the queue
            this.logger.debug("Unexpected receiving [{}] block of file [{}]. Returning it to queue",
                    rdb.getBlockInfo().getBlockIndex(),
                    rdb.getBlockInfo().getFilename());
            self().tell(rdb, sender());
        }
    }

    private void writeData(RefinedDataBlock rdb, FileWritingMeta meta) throws IOException {
        if (!rdb.getLines().isEmpty()) {
            boolean append = rdb.getBlockInfo().getBlockIndex() > Constants.FIRST_DATA_BLOCK_INDEX && meta.created;
            try (BufferedWriter w = new BufferedWriter(
                    new FileWriter(new File(this.destinationFolder, rdb.getBlockInfo().getFilename()), append),
                    CHAR_BUFFER_SIZE)) {

                meta.created = true;
                meta.extractedLinesCount += rdb.getLines().size();
                // TODO: move encoding out from there for better performance
                w.write(encoder.encodeDataBlock(rdb.getLines()));
            }
        }
    }

    private FileWritingMeta findMeta(RefinedDataBlock block) {
        FileWritingMeta meta = this.fileWritingMeta.get(block.getBlockInfo().getFilename());
        if (meta != null) {
            this.logger.debug("Found meta for file [{}]", block.getBlockInfo().getFilename());
            return meta;
        } else if (block.getBlockInfo().getBlockIndex() == Constants.FIRST_DATA_BLOCK_INDEX) {
            meta = new FileWritingMeta(
                    block.getBlockInfo().getFilename(),
                    Constants.FIRST_DATA_BLOCK_INDEX - 1);
            this.fileWritingMeta.put(block.getBlockInfo().getFilename(), meta);

            this.logger.info("Registered writing new file [{}]", block.getBlockInfo().getFilename());

            return meta;
        } else {
            return null;
        }
    }

    private void registerDroppedFile(DropFile df) {
        this.logger.debug("Register dropped file [{}]", df.getFilename());

        FileWritingMeta meta = this.fileWritingMeta.get(df.getFilename());
        if (meta != null) {
            deleteDroppedFile(df.getFilename());
        }

        this.droppedFiles.add(df.getFilename());
    }

    private void deleteDroppedFile(String filename) {
        File f = new File(this.destinationFolder, filename);
        if (f.exists()) {

            this.logger.info("Removing broken file [{}]", filename);
            f.delete();
        }
    }

    private void registerCoworkers(RegisterCoworkers rc) {
        this.logger.debug("Register coworkers");

        this.coworkers = rc.getCoworkers();
    }

    private static class FileWritingMeta {

        FileWritingMeta(String filename, int lastBlockIndex) {
            this.filename = filename;
            this.lastBlockIndex = lastBlockIndex;
            this.extractedLinesCount = 0;
            this.created = false;
            this.closed = false;
        }

        String filename;
        int lastBlockIndex;
        int extractedLinesCount;
        boolean created;
        boolean closed;

    }

}
