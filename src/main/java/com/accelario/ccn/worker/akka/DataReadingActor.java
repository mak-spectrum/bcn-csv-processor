package com.accelario.ccn.worker.akka;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import akka.actor.AbstractActorWithUnboundedStash;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.Router;
import com.accelario.ccn.model.FileStatus;
import com.accelario.ccn.reader.RawDataReader;
import com.accelario.ccn.worker.message.DropFile;
import com.accelario.ccn.worker.message.RawDataBlock;
import com.accelario.ccn.worker.message.ReadFile;
import com.accelario.ccn.worker.message.RegisterCoworkers;

public class DataReadingActor extends AbstractActorWithUnboundedStash {

    private static final int OPEN_FILES_LIMIT = 5; // TODO move me to configuration


    public static Props props(File sourceFolder, Router decodingActors, Router coworkers) {
        return Props.create(DataReadingActor.class,
                () -> new DataReadingActor(sourceFolder, decodingActors, coworkers));
    }

    private final LoggingAdapter logger = Logging.getLogger(getContext().getSystem(), this);
    private final Map<String, RawDataReader> openFiles = new HashMap<>();
    private final Set<String> droppedFiles = new HashSet<>();

    private final File sourceFolder;
    private final Router decodingActors;
    private final Router coworkers;

    DataReadingActor(File sourceFolder, Router decodingActors, Router coworkers) {
        this.sourceFolder = sourceFolder;
        this.decodingActors = decodingActors;
        this.coworkers = coworkers;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ReadFile.class, this::readDataFile)
                .match(DropFile.class, this::registerDroppedFile)
                .match(RegisterCoworkers.class, rc -> {/* not needed */})
                .build();
    }

    private void readDataFile(ReadFile of) throws IOException {
        if (droppedFiles.contains(of.getFilename())) {
            this.logger.debug("Ignored inappropriate file [{}]", of.getFilename());
            this.closeQuietly(this.openFiles.remove(of.getFilename()));
            return;
        }
        RawDataReader dataReader = this.findDataReader(of);

        if (dataReader != null) {
            this.logger.debug("Reading next block from file [{}]", of.getFilename());
            RawDataBlock block = readNextBlock(of, dataReader);
            if (block != null) {
                this.logger.debug("Got block [{}] from file [{}]",
                        block.getBlockInfo().getBlockIndex(), of.getFilename());

                if (block.getBlockInfo().isFinal()) {
                    // file end - drop reader cache
                    this.logger.info("Final block for file [{}] found. Closing reader.", of.getFilename());
                    this.releaseResources(of);
                } else {
                    // tell to self - do reading of next block
                    this.logger.debug("Launching next read for file [{}]", of.getFilename());
                    self().tell(of, self());
                }
                // tell to decoders - do block decoding
                this.logger.debug("Sending block [{}] of file [{}] to decoders",
                        block.getBlockInfo().getBlockIndex(), of.getFilename());

                this.decodingActors.route(block, self());
            } else {
                // file reading exception - drop reader cache
                this.logger.error("Error reading of file [{}]. Drop file system wide.",
                        of.getFilename());

                this.releaseResources(of);
                this.droppedFiles.add(of.getFilename());
                this.coworkers.route(
                        new DropFile(of.getFilename(), FileStatus.ERROR_READING),
                        self()
                );
            }
        }
    }

    private RawDataReader findDataReader(ReadFile of) throws IOException {
        RawDataReader dataReader = this.openFiles.get(of.getFilename());

        if (dataReader == null) {
            if (this.openFiles.size() < OPEN_FILES_LIMIT) {
                // open a reader, if there is space in the pool
                this.logger.info("Opening file [{}]", of.getFilename());

                dataReader = new RawDataReader(this.sourceFolder, of.getFilename());
                this.openFiles.put(of.getFilename(), dataReader);

            } else {
                // stash message file that cannot be read yet
                stash();
            }
        }

        return dataReader;
    }

    private void releaseResources(ReadFile of) {
        this.logger.debug("Releasing resources. Unstashing messages, if any");
        this.closeQuietly(this.openFiles.remove(of.getFilename()));
        unstash();
    }

    // FIXME: need better consistency in open-close logic; it is too complicated now
    private RawDataBlock readNextBlock(ReadFile of, RawDataReader dataReader) {
        try {
            RawDataBlock block = dataReader.readNextBlock();
            if (block.getBlockInfo().isFinal()) {
                this.closeQuietly(dataReader);
            }
            return block;
        } catch (IOException e) {
            this.logger.error("Error reading file [{}] within actor [{}]", of.getFilename(), self(), e);
            this.closeQuietly(dataReader);
            return null;
        }
    }

    private void closeQuietly(RawDataReader reader) {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                // ignore it
            }
        }
    }

    private void registerDroppedFile(DropFile df) {
        this.logger.debug("Register dropped file [{}]", df.getFilename());

        this.droppedFiles.add(df.getFilename());
    }

}
