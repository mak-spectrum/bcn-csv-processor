package com.accelario.ccn.validator;

import org.apache.commons.validator.routines.checkdigit.LuhnCheckDigit;

public class LuhnCcnValidator implements CcnValidator {

    private static final int MIN_BANK_CARD_NUMBER_LENGTH = 12;
    private static final int MAX_BANK_CARD_NUMBER_LENGTH = 19;

    private LuhnCheckDigit luhnCheckDigit = new LuhnCheckDigit();

    @Override
    public boolean isValid(String cellValue) {
        return this.isLengthCompliant(cellValue)
                && consistsOfNumbers(cellValue)
                && passesLuhnCheck(cellValue);
    }

    private boolean isLengthCompliant(String cellValue) {
        return cellValue.length() >= MIN_BANK_CARD_NUMBER_LENGTH
                && cellValue.length() <= MAX_BANK_CARD_NUMBER_LENGTH;
    }

    private boolean consistsOfNumbers(String cellValue) {
        for (int i = 0; i < cellValue.length(); i++) {
            char ch = cellValue.charAt(i);
            if (ch < '0' || ch > '9') {
                return false;
            }
        }

        return true;
    }

    private boolean passesLuhnCheck(String cellValue) {
        return luhnCheckDigit.isValid(cellValue);
    }
}
