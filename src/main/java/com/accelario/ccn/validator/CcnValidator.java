package com.accelario.ccn.validator;

public interface CcnValidator {

    boolean isValid(String cellValue);

}
