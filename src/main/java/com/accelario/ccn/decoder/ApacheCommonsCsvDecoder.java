package com.accelario.ccn.decoder;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.accelario.ccn.model.DataRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class ApacheCommonsCsvDecoder implements CsvDecoder {

    @Override
    public List<DataRecord> parseDataBlock(List<String> rawData) throws IOException {
        try {
            StringReader joinedData = this.prepareRawDataReader(rawData);

            List<DataRecord> result = new ArrayList<>();

            new CSVParser(joinedData, CSVFormat.DEFAULT.withIgnoreSurroundingSpaces(true)).iterator().forEachRemaining(
                    record -> result.add(this.csvRecordToDataRecord(record))
            );

            return result;
        } catch (Exception e) {
            throw new IOException("Error decoding CSV data block.", e);
        }
    }

    private StringReader prepareRawDataReader(List<String> rawData) {
        return new StringReader(
                String.join(CSVFormat.RFC4180.getRecordSeparator(), rawData)
        );
    }

    private DataRecord csvRecordToDataRecord(CSVRecord record) {
        String[] lineResult = new String[record.size()];

        int index = 0;
        Iterator<String> cellIterator = record.iterator();

        while (cellIterator.hasNext()) {
            lineResult[index++] = cellIterator.next();
        }

        return new DataRecord(lineResult);
    }


}
