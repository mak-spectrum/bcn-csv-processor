package com.accelario.ccn.decoder;


import java.io.IOException;
import java.util.List;

import com.accelario.ccn.model.DataRecord;

public interface CsvDecoder {

    List<DataRecord> parseDataBlock(List<String> rawData) throws IOException;

}
